; Copyright (c) 2017 Liav Albani

; Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
; to deal in the Software without restriction, including without limitation the rights to use, copy,
; modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
; subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE
org 0x100
jmp start

;general variables
lastmovement db 100d
length_snake dw 24*2d
length_entities dw 0
;buffer_arr dw 30 dup (0) ; buffering between two arrays.
level_score dw 0


snake_pixels dw 0 ; snake pixel array
times 3000 dw 0
buffer_arrays dw 0 ; buffering between two arrays.
times 300 dw 0

entities_pixels dw 0 ; array structure will be like so: the array will be divided into data blocks - first byte of block will define entity
times 5*50 dw 0
; the second and third byte will together present the place of pixel
; therefore we will save for each data block 3 bytes. this structure will allow us in the future to add more types of entities.
; current types of entities: 1 - bug1, 2 - bug2, 3 - poison
; maximum of 5 entities can spawned at any time.


;; general messages
msg1 db "Please press any key to continue...$"
msg_death db "Your snake is dead. Are you willing to try your best again? ",10d,"Your score was: ",30,"$"
about_msg1 db "Copyright (c) 2017 Liav Albani ",10d,"Permission is hereby granted, free of charge, to any person obtaining a copy of ",10d,"this software and associated documentation files (the 'Software'),to deal in the",10d," Software without restriction, including without limitation the rights to use,",10d
about_msg2 db "copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the ",10d,"Software, and to permit persons to whom the Software is furnished to do so,",10d,"subject to the following conditions:",10d,"The above copyright notice and this",10d,"permission notice shall be included in all copies or substantial portions of",10d,"the Software.",10d,"THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR ",10d,"IMPLIED,INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS ",10d,"FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.",10d,"IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,",10d,"DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,TORT OR OTHERWISE,",10d,"ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER",10d,"DEALINGS IN THE SOFTWARE"
about_msg3 db 10d,"Please press any key to return to menu$"
;; Main menu settings, options & definitions
;menu1_length equ 5h ;length_of_menu1
menu_1 db 0, 3 ; first byte of array is zero, second byte is length of options
start_game db "Start/Continue Game$",0
;view_scores db "View Score$",1
;shop_market db "Shop Market$",2
about_program db "About Program$",1
exit_program db "Exit Game$",2

;few constants for this program
color_of_border equ 21



bug1_color equ 56h ;56h is the color of bug 1
bug2_color equ 72h ;72h is the color of bug 2
poison_color equ 49h ;49h is the color of poison


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Main Menu Procedures -------------------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - offset of message, Offset in video memory(Text Mode - 80*25), Selected(1 or 0)
; Write Messages to Screen according to offset of message & offset in video memory
; Important Note!!: The message has to end with $ sign! otherwise endless loop will occur!!
; Return new address after writing
write_options:
push bp
mov bp,sp

push bx
push di
push dx
push ax

mov bx,[bp+8] ;get offset of message
mov di,[bp+6] ;get offset in text mode video memory
mov ax,[bp+4] ;get selected true or false(1 or 0)

cmp ax,1
je loop_write_char3 ; if the option we are writing is selected we are jumping to the appropriate place
jmp loop_write_char2
loop_write_char2:
	cmp byte [ds:bx],'$' ; check for end of message
	jz end_loop_char3 ; jump if end of message
	mov dl,[ds:bx]
	mov dh,0000101b
	mov [es:di],dx ;copy character to video memory
	inc bx
	add di,2
	jmp loop_write_char2

loop_write_char3:
	cmp byte [ds:bx],'$' ; check for end of message
	jz end_loop_char3 ; jump if end of message
	mov dl,[ds:bx]
	;mov dh,00101010b
	mov dh,11001010b
	mov [es:di],dx ;copy character to video memory
	inc bx
	add di,2
	jmp loop_write_char3
end_loop_char3:

inc bx
mov [bp+8], bx
pop ax
pop dx
pop di
pop bx


pop bp
ret 4

; Parameters - No params
; Write About me. Maybe the most funny thing to do. Please Note: This is not an universal procedure - Do not try to take this proc with thinking there is not change needed before!!
; I haven't planned it because of time limitations, and considering that "About The Program" is a section that is strongly related to my snake game.
; In any case - always make the appropriate changes to make sure the code will fit your needs...!!(P.S. you can always learn from my code & project, that's why I use MIT license, however, I strongly urge you to don't copy paste...)
; Returns nothing
write_about_program:
push bx
push di
push dx
push ax
push cx
	lea bx,[about_msg1]
	; params for write_messages_slow
	mov dx,0
	push bx
	push dx
	xor ax,ax
	push ax
	mov ax,1
	push ax ; request no delay
	call write_messages_slow
	
	mov ah,01
about_program_wait_key:
	int 16h
	jne about_program_exit
	jmp about_program_wait_key
about_program_exit:
pop cx
pop ax
pop dx
pop di
pop bx
ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of Menu Information in Data Segment, Number of Options, Option Selected
; Create Menu(Video Memory)
; Return according to case: 0 - do nothing, 1 - exit game
create_main_menu:
push bp
mov bp,sp
push di
push bx
push cx
push dx
push ax
push si
	mov bx,[bp+8] ; offset
	mov si,[bp+6] ; number of options
	mov dx,[bp+4] ; selected
	call clean_textmode_screen
	
	mov ax,dx ; get where we need to print selected option in the loop...
	mov cx,0 ; will be checked against cx
	mov di, 1960-4*80*2

jmp loop_writing_options
toggle_brushed_option:
mov dx,1
jmp continue_writing_options
loop_writing_options:
	mov dx, 0 ; by default, the option is not brushed(selected) unless change later
	add di,80*2
	inc bx
	;; params for write_options
	push bx
	push di
	cmp ax,cx ; if the option should be brushed as selected, handle it.
	je toggle_brushed_option
	continue_writing_options:
	push dx
	call write_options
	pop bx
	inc cx
	cmp cx,si
	jne loop_writing_options
pop si
pop ax	
pop dx
pop cx
pop bx	
pop di
pop bp
ret 6


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of Menu Information in Data Segment
; Manage Menu(Data Segment)
; Return according to case: 0 - do nothing, 1 - exit game
main_menu:
push bp
mov bp,sp
push dx
push ax
push bx
push cx
push di
push es

	
	xor cx,cx ; cx is our menu option selector
	mov bx,[bp+4]
	
	mov ax, 0003h
	int 10h ;switch to text mode
	
	mov ax,0B800h
	mov es,ax
	;; params for create_main_menu
	inc bx
	push bx
	xor dh,dh
	mov dl,[bx]
	push dx
	push cx
	call create_main_menu
repeat_menu:
	xor ax,ax
	int 16h
	;; params for keyboard_menu_handler
	push ax
	push cx
	xor dh,dh
	mov dl,[bx]
	push dx
	call keyboard_menu_handler
	pop di ;return enter flag
	pop cx ;return updated selector
	cmp di,1
	je handle_enter_flag
	continue_repeat_menu:
	;; params for create_main_menu
	push bx
	xor dh,dh
	mov dl,[bx]
	push dx
	push cx
	call create_main_menu
	jmp repeat_menu
handle_enter_flag:
	; main menu handler: 0 - continue game, 1 - view score, 2 - about program, 3 - exit game
	cmp cx,0
	je exit_main_menu_0
	cmp cx,1
	je about_me
	cmp cx,2
	je exit_main_menu_3
	jmp continue_repeat_menu

about_me:
	call clean_textmode_screen
	call write_about_program
	mov cx,1
	jmp repeat_menu
exit_main_menu_3:
	mov cx,1
	jmp exit_main_menu
exit_main_menu_0:
	mov cx,0
exit_main_menu:
	
	mov [bp+4],cx
	;mov ax, 0013h
	;int 10h ;switch to text mode
	;mov ax,0A000h
	;mov es,ax
	call prepare_game
		
pop es
pop di
pop cx
pop bx
pop ax
pop dx
pop bp
ret


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Key Movement, Menu Option Selector, Limit of Selections
; Manage Menu Keys
; Return according to case: 0 - do nothing, 1 - enter, also return new option selector
keyboard_menu_handler:
push bp
mov bp,sp

push ax
push cx
push di
push dx
mov ax,[bp+8]
mov cx,[bp+6]
mov dx,[bp+4]
dec dx ; doing that because dx is 4, but we count the items of menu from 0..1..2..3..not from 1-4
	mov di, 0 ; do nothing
	cmp ah,48h
	je menu_up
	cmp al,'w'
	je menu_up
	cmp ah,50h
	je menu_down
	cmp ah,1Ch ; enter key
	je menu_enter
	cmp al,'s'
	je menu_down
	jmp menu_select
menu_up:
	cmp cx,0
	je menu_select
	dec cx
	jmp menu_select
menu_down:
	cmp cx,dx
	je menu_select
	inc cx
	jmp menu_select
menu_enter:
	mov di,1
	jmp menu_select
menu_select:
	mov [bp+8],cx ;return updated selector
	mov [bp+6],di ;return enter flag
	
pop dx
pop di
pop cx
pop ax

pop bp
ret 2


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ---------------------------------------------------------------------------- Entities Handling Procedures ----------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of entities array, Offset of entities length array var
; Restore all current entities from entities array
; Returns nothing
restore_entities:
push bp
mov bp,sp
push bx
push cx
push ax
push di

	mov di,[bp+6]
	mov si,[bp+4]
	
	mov cx,[si]
	cmp cx,0
	je skip_restore_entities
	
restore_entities_loop:
	xor dh,dh
	mov dl,byte [ds:di]
	inc di
	mov bx,word [ds:di]
	push bx ; param 1 for draw_cell
	push dx ; param 2 for draw_cell
	call spawn_food
	add di,4
	loop restore_entities_loop

skip_restore_entities:
	
pop di
pop ax
pop cx
pop bx	
pop bp
ret 4


; Parameters - Offset of entities array, Offset of entities length array var
; Save all current entities from entities array
; Returns nothing
save_entities:
push bp
mov bp,sp
push bx
push cx
push ax
push di
push si

mov di,[bp+6]
mov si,[bp+4]

mov word [ds:si],0

xor dx,dx
jmp search_for_entities

save_entity:
mov byte [ds:di], al
inc di
mov word [ds:di],bx
add di,4
add dx,1
jmp continue_search

search_for_entities:
	
	mov cx,320*200

screen_loop:
	mov bx,cx
	mov ax, bug1_color
	cmp byte [es:bx],bug1_color
	je save_entity
	mov ax, bug2_color
	cmp byte [es:bx],bug2_color
	je save_entity
	mov ax, poison_color
	cmp byte [es:bx],poison_color
	je save_entity
	continue_search:
	loop screen_loop

	mov word [ds:si],dx
	
pop si
pop di
pop ax
pop cx
pop bx	
pop bp
ret 4

; Parameters - Type of food(1 - bug1, 2 - bug2, 3 - poison)
; Spawn a new entity in array(in end of array)... only for initialize game...
; Returns nothing
spawn_entity:
push bp
mov bp,sp
push bx
push cx
push ax
push di


; get coordinate for food...
getting_rand_coordinate:
	push bx ; garbage param for random
	call rand_number
	pop bx
; check if a food(or something else, like the snake itself...) is already exist....if yes getting new coordinate.
	cmp byte [es:bx],0
	jne getting_rand_coordinate
	
	mov dx,[bp+4]
	
;spawn_bug1:
	mov ax,bug1_color
	cmp dx,bug1_color
	je spawn_entity_proc
;spawn_bug2
	mov ax,bug2_color
	cmp dx,bug2_color
	je spawn_entity_proc
;spawn_poison
	mov ax,poison_color
	cmp dx,poison_color
	je spawn_entity_proc
;failure to spawn due to error in sending parameters...
	jmp exit_spawn
spawn_entity_proc:
	
	push bx ; param 1 for draw_cell
	push ax ; param 2 for draw_cell
	call spawn_food
	
exit_spawn:

pop di
pop ax
pop cx
pop bx	
pop bp
ret 2

; Parameters - Address of Pixel(ES), Color
; Draw food on screen
; Returns nothing
spawn_food:
push bp
mov bp,sp
push bx
push ax

	
	mov bx,[bp+6] ;get address
	mov ax,[bp+4] ;get color
	;mov ax,bug1_color
	
	;draw center
	mov [es:bx], al ; draw our entity
	
	
pop ax
pop bx
pop bp
ret 4


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Snake Initializition Procedures --------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; Parameters - Offset of snake array, Length of array, Offset of snake array in video memory
; Create Snake array in memory(Data Segment)
; Return nothing
init_snake_memory:
push bp
mov bp,sp

push bx
push cx
push di

mov bx,[bp+8]
mov cx,[bp+6]
mov di,[bp+4]

loop_memory_init_snake:
	mov [ds:bx],di
	
	add bx,2
	add di,1
	;add di,2
	loop loop_memory_init_snake

pop di
pop cx
pop bx
	
pop bp
ret 6

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Random Pixel Procedures --------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Blank Parameter(Can be anything)
; Create Random number for many purposes(creating bugs, poison...). The procedure creates coordinates inside the frame
; Return random number
rand_y_coordinate:
push bp
mov bp,sp

push ax
push bx
push cx
push di
push es

mov cx,[bp+4]

mov ax,40h ; prepare offset for taking random number
mov es,ax

rand_y:
mov ax,[es:6Ch]
and ax,128; random from 0 to 128 
mov dx, ax

mov ax,[es:6Ch]
and ax,64; random from 0 to 64

add ax,dx
cmp ax,188 ; maximum is 188 - getting the row in screen(by calculation of 200-6*2=188) so we get 188 pixels without borders & two lines beyond that
ja rand_y

mov [bp+4],ax

pop es
pop di
pop cx
pop bx
pop ax

pop bp
ret ; get parameter & return one value

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Blank Parameter(Can be anything)
; Create Random number for many purposes(creating bugs, poison...). The procedure creates coordinates inside the frame
; Return random number
rand_x_coordinate:
push bp
mov bp,sp

push ax
push bx
push cx
push di
push es

mov cx,[bp+4]

mov ax,40h ; prepare offset for taking random number
mov es,ax

rand_x:
mov ax,[es:6Ch]
and ax,11111111b; random from 0 to 256 
mov dx, ax

mov ax,[es:6Ch]
and ax,00111111b; random from 0 to 64

add ax,dx
cmp ax,308 ; maximum is 308 - getting the row in screen(by calculation of 320-6*2 = 310) so we get 188 pixels without borders & two lines beyond that
ja rand_x

mov [bp+4],ax

pop es
pop di
pop cx
pop bx
pop ax

pop bp
ret ; get parameter & return one value


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Blank Parameter(Can be anything)
; Create Random number for many purposes(creating bugs, poison...). The procedure creates coordinates inside the frame
; Return random number
rand_number:
push bp
mov bp,sp

push ax
push bx
push cx
push di
push dx
push es

mov cx,[bp+4]
xor cx,cx

mov ax,40h ; prepare offset for taking random number
mov es,ax

operation1:
push cx ; send blank parameter
call rand_x_coordinate
pop dx
operation2:
push cx ; send blank parameter
call rand_x_coordinate
pop ax

mov di, 6*320 ; set offset of location in lines
mov bx, 6 ; set offset of location in rows

add bx,di ;get one offset....

mul dx ;get coordinate by multiply ax with dx. can get from 0 to 58900

add ax, bx ; add offset. so the address can be from 0+5*320+5 to 58900+5*320+5

mov [bp+4],ax

pop es
pop dx
pop di
pop cx
pop bx
pop ax

pop bp
ret ; get parameter & return one value

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Snake Interacts Procedures -------------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Offset of Length of snake array
; Increase snake length by 3
; Returns nothing
increase_snake_length:
push bp
mov bp,sp
push bx
push di
push ax
push cx
push si
push dx

mov di,[bp+6]
mov bx,[bp+4]

mov dx,[bx]

mov ax,2
mul dx

add di,ax

mov ax,[ds:di] ; get last address of snake, move it to ax & decrease it - to prepare it as a new cells...
;mov dx,[ds:di-2] ; get last address of snake, move it to ax & decrease it - to prepare it as a new cells...

mov cx,3

creating_new_cells:
	add di,2
	mov [ds:di],ax
	loop creating_new_cells

add word [bx],1

pop dx
pop si
pop cx
pop ax
pop di
pop bx
pop bp
ret 4

; Parameters - Offset of snake array, Offset of Length of snake array
; Decrease snake length by 3
; Returns nothing
decrease_snake_length:
push bp
mov bp,sp
push bx
push di
push ax
push si
push dx
push cx

mov ax,2

mov di,[bp+6]
mov bx,[bp+4]

mov cx,[bx]

mul cx

add bx,ax
mov cx,3

loop_clear_old_cells:
	add bx,2
	mov dx,word [ds:bx]
	mov si,dx
	mov byte [es:si],0
loop loop_clear_old_cells

cmp word [bx],24*2d
jbe exit_decrease_snake_length
sub word [bx],1
exit_decrease_snake_length:

pop cx
pop dx
pop si
pop ax
pop di
pop bx
pop bp
ret 4



; Parameters - Offset of snake array, Length of snake array, Last Movement
; Perform required tasks with entities & interacts in our game
; Return according to case: encounter a bug 1 - 1, encounter a bug 2 - 2, encounter a poison - 3, encounter a border/snake - 4
interact_handler:
push bp
mov bp,sp

	push bx
	push cx
	push di
	push dx
	mov bx,[bp+8] ; get offset of snake array
	mov cx,[bp+6] ; get length of snake array
	mov dx,[bp+4] ; get last movement
	; params for check_for_case
	push dx
	push bx
	push cx
	call check_for_case
	pop di ; get returned value
	
interacts_exit:
	mov [bp+8],di
	
	pop dx
	pop di
	pop cx
	pop bx
	
pop bp
ret 4


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; Parameters - Status Integer output(from interact_handler) ,Offset of Score, Offset of snake array, Offset of Length of snake array
; Perform required tasks in our game(increasing score & spawning more food)
; Returns nothing
level_handler:
push bp
mov bp,sp
push bx
push si
push di
push cx
push ax
push dx
	mov ax,[bp+6]
	mov si,[bp+4]
	
	jmp check_status_handle
case_bug1_interact:
	add word [ds:si],3 ; increase score
	mov cx,bug2_color
	push cx
	call spawn_entity
	mov cx,bug1_color
	push cx
	call spawn_entity
	

	
	jmp exit_level_handler
case_bug2_interact:
	add word [ds:si],6 ; increase score
	mov cx,bug1_color
	push cx
	call spawn_entity
	mov cx,poison_color
	push cx
	call spawn_entity
	mov cx,bug2_color
	push cx
	call spawn_entity
	
	
	jmp exit_level_handler
case_poison_interact:
	mov cx,bug1_color
	push cx
	call spawn_entity
	sub word [ds:si],3 ; decrease score

	
	jmp exit_level_handler
	
check_status_handle:	
	
	cmp ax,bug2_color
	je case_bug2_interact
	cmp ax,poison_color
	je case_poison_interact
	cmp ax,bug1_color
	je case_bug1_interact
	
exit_level_handler:
pop dx
pop ax
pop cx
pop di
pop si
pop bx
pop bp
ret 4

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Snake Keyboard Handle Procedures -------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Moving action(key input),Offset of snake array, Length of array
; Exchange between cases according to moving of head pixel of snake. If the procedure finds that head will encounter a bug, it will call it automatically.
; bug reward event otherwise, if encountered a poison it will call poison event, and if encountered a border, will return to procedure a value so
; the procedure can exit the program afterwards nicely and tell the player that he failed...
; Return According to case: unset operation - 0 ,encounter a bug 1 - 1, encounter a bug 2 - 2, encounter a poison - 3, encounter a border/snake - 4
check_for_case:
	push bp
	mov bp,sp

	push ax
	push dx
	push di
	push cx
	push bx
	
	
	mov dx,[bp+8] ; get action(key input INT 16H)
	mov bx,[bp+6] ;get offset of array
	mov cx,[bp+4] ;get length of array
	mov di,[bx] ; get head pixel address in video memory...
	; start switch cases....(like old school days in C# but now have some fun)
	
	
	cmp dl,'w'
	je case_up
	cmp dl,'a'
	je case_left
	cmp dl,'d'
	je case_right
	cmp dl,'s'
	je case_down
	mov di,0 
	jmp unset_operation ; exit without doing anything
case_up:
	;params for check_encounter_up
	sub di,320 ; case up...
	push di
	push bx
	push cx
	jmp finish_cases
case_down:
	;params for check_encounter_down
	add di,320 ; case down...
	push di
	push bx
	push cx
	jmp finish_cases
case_left:
	;params for check_encounter_left
	sub di,1 ; case left...
	push di
	push bx
	push cx
	jmp finish_cases
case_right:
	;params for check_encounter_right
	add di, 1 ; case right...
	push di
	push bx
	push cx
	jmp finish_cases
finish_cases:
	
	call check_eating_entities
	pop di ; get returned state from procedure
	mov [bp+8],di ; return sign of case
unset_operation:
	
	pop bx
	pop cx
	pop di
	pop dx
	pop ax
		
	pop bp
ret 4 ; return + 4 so we can return one value to program

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Address of Targeted New Head Pixel,Offset of snake array, Length of array
; Check if Snake is eating himself(Checking against Data Segment, in snake array)
; Return According to case: encounter a snake - 1, not encountering - 0
check_eating_snake:
	push bp
	mov bp,sp
	push bx
	push cx
	push di
	push dx
	mov bx,[bp+8]
	mov di,[bp+6]
	mov cx,[bp+4]
	
	mov dx,0
loop_check_for_encountering:
	cmp bx,[ds:di]
	je exit_check_for_encounter_with_1
	add di,2
	loop loop_check_for_encountering
jmp exit_check_for_encounter
exit_check_for_encounter_with_1:
	mov dx,1
exit_check_for_encounter:
	mov [bp+8], dx
	
	pop dx
	pop di
	pop cx
	pop bx
	pop bp
ret 4 ;return one value to program

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - address of target pixel(Extra Segment in Pixel Mode),Offset of snake array, Length of array
; Check eating entities
; Return According to case: encounter a bug 1 - 1, encounter a bug 2 - 2, encounter a poison - 3, encounter a border/snake - 4
check_eating_entities:
	push bp
	mov bp,sp
	push bx
	push di
	push cx
	push si
	
	mov bx,[bp+8] ;get address of target pixel...
	mov si,[bp+6] ;get offset of snake array
	mov cx,[bp+4] ;get length of snake array
	
	;params for check_encounter_snake
	push bx ;target of head pixel
	push si ;offset of snake array
	push cx ;length of snake array
	call check_eating_snake
	pop cx ;get returned value
	cmp cx,1 ;1 is the indicator of encountering a snake
	je case_border_snake
	cmp byte [es:bx],color_of_border ;21 is the color of border
	je case_border_snake
	cmp byte [es:bx],bug1_color
	je case_bug1
	cmp byte [es:bx],bug2_color
	je case_bug2
	cmp byte [es:bx],poison_color
	je case_poison

	jmp finish_cases_2
case_border_snake: ; same exception for both border & snake as they result in death...
	mov di, color_of_border
	jmp finish_cases_2
case_bug1:
	mov di, bug1_color
	jmp finish_cases_2
case_bug2:
	mov di, bug2_color
	jmp finish_cases_2
case_poison:
	mov di, poison_color
	jmp finish_cases_2
finish_cases_2:
	mov [bp+8],di ; return sign of case
	pop si
	pop cx
	pop di
	pop bx
	pop bp
ret 4

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Draw Borders Procedures ----------------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Create Borders in video memory
; Return nothing
draw_borders:
	call draw_top_border
	call draw_bottom_border
	call draw_left_border
	call draw_right_border
ret

; Parameters - nothing
; Create Top Border in video memory
; Return nothing
draw_top_border:
	push cx
	push bx
	push dx
	mov dl, color_of_border
	mov cx, 320*4-1 ;will paint 320 pixels

	mov bx,0h
	mov [es:bx], dl ; paint to first pixel
loop_border1: ; loop all pixel in top border
	mov bx, cx
	mov [es:bx], dl
	loop loop_border1
	pop dx
	pop bx
	pop cx
ret
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Create Bottom Border in video memory
; Return nothing
draw_bottom_border:
	push cx
	push bx
	push dx
	mov dl, color_of_border
	mov cx,320*200
	mov bx,320*200-1
	mov [es:bx], dl ; paint to last pixel
loop_border2: ; loop all pixel in bottom border
	mov bx, cx
	mov [es:bx], dl
	dec cx
	cmp cx, 320*200-320*4-1
	jne loop_border2
	pop dx
	pop bx
	pop cx
ret
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Create Left Border in video memory
; Return nothing
draw_left_border:
	push cx
	push bx
	push dx
	mov dl, 21
	mov cx,320*200
loop_border3: ; loop all pixel in right & left borders
	mov bx,cx
	mov [es:bx], dl
	inc bx
	mov [es:bx], dl
	inc bx
	mov [es:bx], dl
	inc bx
	mov [es:bx], dl
	sub cx,320
	cmp cx,0
	jne loop_border3
	pop dx
	pop bx
	pop cx
ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Create Right Border in video memory
; Return nothing
draw_right_border:
	push cx
	push bx
	push dx
	mov dl, color_of_border
	mov cx,320
loop_border4: ; loop all pixel in right & left borders
	mov bx,cx
	dec bx
	mov [es:bx], dl
	dec bx
	mov [es:bx], dl
	dec bx
	mov [es:bx], dl
	dec bx
	mov [es:bx], dl
	add cx,320
	cmp cx,320*200
	jne loop_border4
	pop dx
	pop bx
	pop cx
ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Snake Movement Procedures --------------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of array, Head Pixel Reserve
; Move snake parts in array in data segment memory
; Return nothing
move_array_cells:
push bp
mov bp,sp

push ax
push bx
push cx
push di

mov bx,[bp+8]
mov cx,[bp+6]
mov di,[bp+4]
xor ax,ax ; AX register will be used as a temporary register for loop working on moving cells

loop_move_snake_cells:
	add bx,2 ; skipping to next cell in array(jumping two bytes as this array is defined as word)
	mov ax,[ds:bx]
	mov [ds:bx],di
	mov di,ax
	loop loop_move_snake_cells

pop di
pop cx
pop bx
pop ax

pop bp
ret 6

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of array
; Move up Snake
; Return nothing
move_up:
	push bp
	mov bp,sp
	push cx
	push bx
	push dx
	push ax
	
	mov bx,[bp+6]
	mov cx,[bp+4]
	
	
	mov ax,[bx] ;move first pixel(or offset) to AX
	mov dx,ax ;copy offset to DX, DX will reserve our offset
	
	sub ax,320 ; move first pixel down
	mov [bx],ax
	
	; params of move_array_cells procedure
	push bx ; offset of array parameter
	push cx ; length of array parameter
	push dx ; offset reserve of array parameter
	call move_array_cells
	pop ax
	pop dx
	pop bx
	pop cx
	pop bp
ret 4 ; return one value

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of array
; Move down Snake
; Return nothing
move_down:
	push bp
	mov bp,sp
	push cx
	push bx
	push dx
	push ax
	
	mov bx,[bp+6]
	mov cx,[bp+4]
	
	mov ax,[bx] ;move first pixel(or offset) to AX
	mov dx,ax ;copy offset to DX, DX will reserve our offset
	
	add ax,320	; move first pixel down
	mov [bx],ax
	
	; params of move_array_cells procedure
	push bx ; offset of array parameter
	push cx ; length of array parameter
	push dx ; offset reserve of array parameter
	call move_array_cells
	pop ax
	pop dx
	pop bx
	pop cx
	pop bp
ret 4 ; return one value

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of array
; Move right Snake
; Return nothing
move_right:
	push bp
	mov bp,sp
	push cx
	push bx
	push dx
	push ax
	
	mov bx,[bp+6]
	mov cx,[bp+4]
	
	mov ax,[bx] ;move first pixel(or offset) to AX
	mov dx,ax ;copy offset to DX, DX will reserve our offset
	
	add ax,1 ; move first pixel to right
	mov [bx],ax
	
	; params of move_array_cells procedure
	push bx ; offset of array parameter
	push cx ; length of array parameter
	push dx ; offset reserve of array parameter
	call move_array_cells
	pop ax
	pop dx
	pop bx
	pop cx
	pop bp
ret 4 ; return one value
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of array
; Move left Snake
; Return nothing
move_left:
	push bp
	mov bp,sp
	push cx
	push bx
	push dx
	push ax
	
	mov bx,[bp+6]
	mov cx,[bp+4]
	
	mov ax,[bx] ;move first pixel(or offset) to AX
	mov dx,ax ;copy offset to DX, DX will reserve our offset
	
	sub ax,1 ; move first pixel to left
	mov [bx],ax
	
	; params of move_array_cells procedure
	push bx ; offset of array parameter
	push cx ; length of array parameter
	push dx ; offset reserve of array parameter
	call move_array_cells
	pop ax
	pop dx
	pop bx
	pop cx
	pop bp
ret 4 ; return one value
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Screen Handling Procedures -------------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of snake(snake array)
; Draw Snake on Video Memory according to Snake array in Data Segment
; Return nothing
draw_snake: ; draw snake
	push bp
	mov bp,sp
	push bx
	push cx
	push ax
	push si
	push di
	
	mov bx,[bp+6] ; get parameter one which is the offset of snake array
	mov cx,[bp+4] ; get parameter two which is the length of snake array(to CX therefore we set up counter for loop)
	xor ah,ah
	
	
	
	dec cx ; we already handled the first cell of array, only need to handle the rest
;push bx
	add bx,2
	mov al,47
loop_draw_snake:
	;mov di,[bx] ; get address of pixel from snake array
	;mov [es:di], al ; draw our snake
	push bx
	push ax
	call draw_cell
	
	add bx,2 ; move a step of word(2 bytes) in our array(which is 2 bytes)
	loop loop_draw_snake
;pop bx

	mov di,[bp+6]
	mov al,0004h
	;mov [es:di], al
	push di
	push ax
	call draw_cell
	 ; move a step of word(2 bytes) in our array(which is 2 bytes) after head of snake
	
	
	pop di
	pop si
	pop ax
	pop cx
	pop bx
	pop bp
ret 4
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; Parameters - Center of cell address, color
; Draw Snake Cell in Screen
; Return nothing

draw_cell:
	push bp
	mov bp,sp
	push bx
	push ax
	
	mov bx,[bp+6] ;get address
	mov ax,[bp+4] ;get color
	xor ah,ah
	;draw center
	mov di,[bx] ; get address of pixel from snake array
	mov [es:di], al ; draw our snake
	
	
	pop ax
	pop bx
	pop bp
ret 4

;proc draw_cell
;	push bp
;	mov bp,sp
;	push bx
;	push ax
;	
;	mov bx,[bp+6] ;get address
;	mov ax,[bp+4] ;get color
;	xor ah,ah
;	;draw center
;	mov di,[bx] ; get address of pixel from snake array
;	mov [es:di], al ; draw our snake
;	;draw upper-right corner
;	mov di,[bx] ; get address of pixel from snake array
;	sub di,320
;	add di,1
;	mov [es:di], al ; draw our snake
;	;draw upper-left corner
;	mov di,[bx] ; get address of pixel from snake array
;	sub di,320
;	sub di,1
;	mov [es:di], al ; draw our snake
;	;draw down-left corner
;	mov di,[bx] ; get address of pixel from snake array
;	add di,320
;	sub di,1
;	mov [es:di], al ; draw our snake
	;draw down-right corner
;	mov di,[bx] ; get address of pixel from snake array
;	add di,320
;	add di,1
;	mov [es:di], al ; draw our snake
	;draw right side
;	mov di,[bx] ; get address of pixel from snake array
;	add di,1
;	mov [es:di], al ; draw our snake
;	;draw left side
;	mov di,[bx] ; get address of pixel from snake array
;	sub di,1
;	mov [es:di], al ; draw our snake
	
	;draw down side
;	mov di,[bx] ; get address of pixel from snake array
;	add di,320
;	mov [es:di], al ; draw our snake
	;draw left side
;	mov di,[bx] ; get address of pixel from snake array
;	sub di,320
;	mov [es:di], al ; draw our snake
	
;	pop ax
;	pop bx
;	pop bp
;ret 4
;endp draw_cell

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of snake(snake array)
; Clean Snake from Video Memory according to Snake array in Data Segment
; Return nothing
clean_snake:
	push bp
	mov bp,sp
	push bx
	push cx
	push ax
	push si
	push di
	
	mov bx,[bp+6] ; get parameter one which is the offset of snake array
	mov cx,[bp+4] ; get parameter two which is the length of snake array(to CX therefore we set up counter for loop)
	
	mov di,[bx]
	mov al,0h
	mov [es:di], al
	
	add bx,2 ; move a step of word(2 bytes) in our array(which is 2 bytes) after head of snake
	dec cx ; we already handled the first cell of array, only need to handle the rest
loop_clean_snake:
	;mov di,[bx] ; get address of pixel from snake array
	;mov [es:di], ah ; draw our snake
	push bx
	push ax
	call draw_cell
	
	add bx,2 ; move a step of word(2 bytes) in our array(which is 2 bytes)
	loop loop_clean_snake
	
	pop di
	pop si
	pop ax
	pop cx
	pop bx
	pop bp
ret 4

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Keyboard Handling & Validation Procedures ----------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Offset of snake array, Length of snake(snake array), Key Input(from INT 16H, BIOS FUNCTION)
; Exchange between operations of snake according to key input
; Return nothing
keyboard_handle:
	push bp
	mov bp,sp
	push bx
	push cx
	push dx
	push ax
	push di
	; retrieve parameters
	;mov si,[bp+16] ; Offset of bugs1 array
	;mov di,[bp+14] ; Offset of bugs2 array
	;mov ax,[bp+12] ; Offset of poison array
	mov bx,[bp+8] ; Offset of snake array
	mov dx,[bp+6] ; Length of snake(snake array)
	mov cx,[bp+4] ; Key Input(from INT 16H, BIOS FUNCTION)
jmp check_Key
movUp:
	;;params for move_up
	push bx ;offset of array
	push dx ;length of snake
	call move_up
	jmp exit_handle
movDown:
	;;params for move_down
	push bx ;offset of array
	push dx ;length of snake
	call move_down
	jmp exit_handle
movRight:
	;;params for move_right
	push bx ;offset of array
	push dx ;length of snake
	call move_right
	jmp exit_handle
movLeft:
	;;params for move_left
	push bx ;offset of array
	push dx ;length of snake
	call move_left
	jmp exit_handle
check_Key:
	
	;;params for clean_snake
	push bx ;offset of array
	push dx ;length of snake
	call clean_snake
	cmp cl,119 ;move up on 'w'
	je movUp
	

	cmp cl,87 ;move up on 'w'
	je movUp
	
	
	cmp cl,115 ;move down on 's'
	je movDown
	
	cmp cl,83 ;move down on 's'
	je movDown
	
	cmp cl,68 ;move right on 'd'
	je movRight
	
	cmp cl,100 ;move right on 'd'
	je movRight
	
	cmp cl,65 ;move left on 'a'
	je movLeft
	
	cmp cl,97 ;move left on 'a'
	je movLeft
exit_handle:
	
	;;params for draw_snake
	push bx ; pass offset address of array
	push dx ; pass length of snake(array snake)
	call draw_snake
	pop di
	pop ax
	pop dx
	pop cx
	pop bx
	pop bp
ret 6
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Last Movement, Entered Key(AX register after INT 16H...)
; Check validity of Entered Key. If key is valid, returns Entered Key - otherwise returns last key valid(last movement)
; Return nothing
check_valid_key:
	push bp
	mov bp,sp
	push bx
	push ax
	push dx
	mov dx,[bp+6] ; Last Movement
	mov cx,[bp+4] ; Entered Key
	
	cmp dx,cx ; in case they are the same(equal)...
	je equal_or_invalid
	
	; default keys...(stored in AL)
	cmp cl,'w'
	je case_w
	cmp cl,'a'
	je case_a
	cmp cl,'s'
	je case_s
	cmp cl,'d'
	je case_d
	; We add native support to arrow keys(stored in AH or just the high priority bits of registers...).
	; We will treat each arrow key as to WASD so no code is needed after this procedure
	cmp ch,48h
	je case_w
	cmp ch,4Bh
	je case_a
	cmp ch,4Dh
	je case_d
	cmp ch,50h
	je case_s
	; finally this is not a movement, but two different keys of control flow(exit program...)
	cmp cl,20h ; hex ascii for space bar
	je esc_or_space
	cmp cl,1Bh ; hex ascii for esc
	je esc_or_space
	; if we reached here and still there is no matching, the user entered an invalid key(which we don't use in this program)
equal_or_invalid: ; also can be used for invalid key input
	mov ax,dx
	jmp return_key
case_w:
	mov ax,'w'
	jmp return_key
case_a:
	mov ax,'a'
	jmp return_key
case_d:
	mov ax,'d'
	jmp return_key
case_s:
	mov ax,'s'
	jmp return_key
esc_or_space:
	mov ax,20h 
	jmp return_key

return_key:
	mov [bp+6],ax ; return key...
	pop dx
	pop ax
	pop bx
	pop bp
ret 2
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Last Movement, Entered Key(AX register after INT 16H...)
; Check against overlap snake. For example, if I move left with snake, I can't move right without going up or down...
; Return nothing
protect_overlap_snake:
	push bp
	mov bp,sp
	push ax
	push cx
	push dx
	mov dx,[bp+6] ; Last Movement
	mov cx,[bp+4] ; Entered Key
	mov ax,cx ; temp register...
	sub ax,dx ; subtract
	cmp ax, 4 ; 4 means that Entered Key minus Last Movement = 4, or Entered key is 'w' and Last Movement is 's' 
	je protect_vs_overlap
	cmp ax, -4 ; 4 means that Entered Key minus Last Movement = -4, or Entered key is 's' and Last Movement is 'w' 
	je protect_vs_overlap
	cmp ax, 3 ; 4 means that Entered Key minus Last Movement = 3, or Entered key is 'd' and Last Movement is 'a' 
	je protect_vs_overlap
	cmp ax, -3 ; 4 means that Entered Key minus Last Movement = -3, or Entered key is 'a' and Last Movement is 'd' 
	je protect_vs_overlap
	mov ax,cx
	jmp return_key_2
protect_vs_overlap:
	mov ax,dx
return_key_2:
	mov [bp+6],ax ; return key...
	pop dx
	pop cx
	pop ax
	pop bp
ret 2

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - Last Movement, Entered Key(AX register after INT 16H...)
; Check & perform a few validations on action of snake
; Return nothing
valid_action_snake:
	push bp
	mov bp,sp
	push ax
	push cx
	push dx
	push bx
	mov dx,[bp+6] ; Last Movement
	mov cx,[bp+4] ; Entered Key
	
	;; params for check_valid_key
	push dx
	push cx
	call check_valid_key
	pop ax ; return after validation procedure 1
	;; params for protect_overlap_snake
	mov bx,ax ; protect ax for a moment
	xor ax,ax ; clear ax
	mov al,bl ; move only required info to stack - just the ascii and not AH which contain garbage for us.....
	push dx
	push ax
	call protect_overlap_snake
	pop dx ; return after validation procedure 2
	
	mov [bp+6],dx ; return finally the appropriate action
	
	pop bx
	pop dx
	pop cx
	pop ax
	pop bp
ret 2

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ----------------------------------------------------------------------------- Screen Messaging Procedures -----------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - offset of message, Offset in video memory(Text Mode - 80*25), Offset of score, Skip Delay
; Write Messages to Screen according to offset of message & offset in video memory
; Important Note!!: The message has to end with $ sign! otherwise endless loop will occur!!
; Return nothing
write_messages_slow:
push bp
mov bp,sp

push bx
push di
push dx
push ax
push si
push cx

mov bx,[bp+10];get offset of message
mov di,[bp+8];get offset in text mode video memory
mov si,[bp+6] ;get offset of score
mov ax,[bp+4] ;check if delay is requested or not(1 or 0)

mov cx,di ; cx will keep state of lines...

jmp loop_write_char

newline:
add cx,80*2
mov di,cx
inc bx
jmp cont_write_chars

score_print:
	mov dx,word [si]
	push dx ; garbage parameter
	push dx ; garbage parameter
	push dx ; garbage parameter
	call convertnumber
	pop si 
	pop ax
	pop dx
push bx
	mov bx,dx
	
	mov dl,bl
	mov dh,09h
	mov [es:di],dx ;copy character to video memory
	
	mov dl,bh
	mov dh,09h
	mov [es:di+2],dx ;copy character to video memory
	
	
	mov dl,al
	mov dh,09h
	mov [es:di+4],dx ;copy character to video memory
	
	mov dl,ah
	mov dh,09h
	mov [es:di+6],dx ;copy character to video memory
	
	mov bx,si
	
	mov dl,bl
	mov dh,09h
	mov [es:di+8],dx ;copy character to video memory
	
	mov dl,bh
	mov dh,09h
	mov [es:di+10],dx ;copy character to video memory
	
pop bx
	;mov dl,ah
	;mov dh,09h
	;mov [es:di+2],dx ;copy character to video memory
	add di,2
	

jmp cont_write_wt_chars

	
loop_write_char:
	cmp byte [ds:bx],'$' ; check for end of message
	jz end_loop_char ; jump if end of message
	cmp byte [ds:bx],10d ; new line
	je newline
	cmp byte [ds:bx],30d ; parse score...
	je score_print
	cont_write_chars:
	mov dl,[ds:bx]
	mov dh,09h
	mov [es:di],dx ;copy character to video memory
	cmp ax,1 ; check if delay is requested
	je skip_delay_writing
	call general_timetaker ;delay writing a bit
skip_delay_writing:
	add di,2
	cont_write_wt_chars:
	inc bx
	jmp loop_write_char
end_loop_char:

pop cx
pop si
pop ax
pop dx
pop di
pop bx

pop bp
ret 8

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ---------------------------------------------------------------------------- Game General Purpose Handling Procedures ----------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;   Paramaters: Number to be converted
;   Calculate number to ascii format
;   Returns Converted number

convertnumber:
push bp
mov bp,sp
    push ax            ; Save modified registers
    push bx
    push dx
	push di
	push cx
	
	
	
	;mov ax,[bp+6]
	mov ax,[bp+6]
	mov bx,10

	;add bp,4
	;mov cx,3
loop_convert_number:
    xor dx, dx         ; Clear dx for division
    div bx            ; Divide by base
    add dl, '0'        ; Convert to printable char
    mov byte [bp+5],dl
	;mov [bp+
	;inc bp
	
	xor dx, dx         ; Clear dx for division
    div bx            ; Divide by base
    add dl, '0'        ; Convert to printable char
    mov byte [bp+4],dl
	;inc bp
	
	xor dx, dx         ; Clear dx for division
    div bx            ; Divide by base
    add dl, '0'        ; Convert to printable char
    mov byte [bp+7],dl
	;inc bp
	
	xor dx, dx         ; Clear dx for division
    div bx            ; Divide by base
    add dl, '0'        ; Convert to printable char
    mov byte [bp+6],dl
	
	xor dx, dx         ; Clear dx for division
    div bx            ; Divide by base
    add dl, '0'        ; Convert to printable char
    mov byte [bp+9],dl
	;inc bp
	
	xor dx, dx         ; Clear dx for division
    div bx            ; Divide by base
    add dl, '0'        ; Convert to printable char
    mov byte [bp+8],dl
	;loop loop_convert_number

pop cx
pop di
pop dx             ; Restore modified registers
pop bx
pop ax
pop bp
ret

; Parameters - Offset of Welcome Message
; Initialize Welcome Message & Enter game
; Return nothing
prompt_for_start: ; prompt for start game
push bp
mov bp,sp
	push bx
	push dx
	push ax
	
	; params for write_messages_slow procedure
	mov bx,[bp+4] ;pass offset of welcome message
	mov ax,1960d ; pass offset in video memory(Text Mode - middle of screen)
	; push parameters
	push bx
	push ax
	xor ax,ax
	push ax ; garbage...don't need really to send offset of score var...
	push ax ; request delay
	call write_messages_slow
    ;mov  dx, bx
	;mov  ah, 9        ; ah=9 - "print string" sub-function
    ;int  21h
	
	mov ah,0h
	mov al,0h
loop_wait_for_prompt:
	int 16h
	cmp al,0
	je loop_wait_for_prompt
	pop ax
	pop dx
	pop bx
pop bp
ret 2

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Come back to text mode(80*25)
; Return nothing
exit_game: ; draw snake game

	push ax
	mov ax, 0003h
	int 10h ;switch to text mode
	pop ax

ret
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Initialize Video Memory in Extra Segment & moving to pixel mode(320*200)
; Return nothing
prepare_game: ; prepare snake game
	push ax
	mov ax, 0013h
	int 10h ;switch to pixel mode
	mov ax, 0A000h
	mov es, ax
	
	pop ax
ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Clean Extra Segment(Video memory) in text mode
; Return nothing
clean_textmode_screen:
	push cx
	push di
	push dx
	mov cx,2001 ;do that 2001 times because we need to clean the screen also in 0, in the start of the screen
	mov dh,0h ; set up the cleaner
	mov dl,' ' ; set up the cleaner
	mov di,4000 ; move DI(ES Data Pointer) to end of screen
clean_scr_loop:
	mov [es:di],dx ;clear screen
	sub di,2 ;go backwards from end of screen to start
	loop clean_scr_loop ; loop until zero
	pop dx
	pop di
	pop cx
ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Initialize Text Mode for prompting in Extra Segment
; Return nothing
init_game: ; initialize game
	push ax
	mov ax,0003h
	int 10h
	mov ax, 0A800h
	mov es, ax
	call clean_textmode_screen ; clear the screen
	pop ax
ret


; Parameters - Offset of Snake Array, Length of Snake Array,Offset in Video Memory
; Initialize Snake Array in Data Segment & Video Segment
; Return nothing
init_snake:
push bp
mov bp,sp
	push bx
	push dx
	push cx
	
	mov bx,[bp+8] ; Offset of Snake Array
	mov cx,[bp+6] ; Length of Snake Array
	mov dx,[bp+4] ; Offset in Video Memory
	;;params for init_snake_memory
	push bx ; push snake array offset(data segment)
	push cx ; push length array
	push dx ; push offset in video memory(extra segment)
	call init_snake_memory ;prepare snake memory
	
	;;params for draw_snake
	push bx ; pass offset address of array
	push cx ; pass length of snake(array snake)
	call draw_snake

	pop cx
	pop dx
	pop bx
pop bp
ret 6

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - offset of message, Offset of Score
; Create delay of half a second approximately
; Return nothing
death_msg:
push bp
mov bp,sp
	push bx
	push dx
	push es
	push di
	push ax
	
	mov bx,[bp+6]
	mov di,[bp+4]
	
	mov ax, 0B800h
	mov es, ax
	mov ax, 0003h
	int 10h
	call clean_textmode_screen
	; params for write_messages_slow
	mov dx,1936
	push bx
	push dx
	push di
	xor ax,ax
	push ax ; request delay
	call write_messages_slow
	
	xor ax,ax
wait_for_key:
	int 16h
	cmp al,0
	je wait_for_key
	
	pop ax
	pop di
	pop es
	pop dx
	pop bx
pop bp
ret 4
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parameters - nothing
; Create delay of half a second approximately, used for management and flow of program
; Return nothing
general_timetaker:
	push cx
	
	mov cx,0FFFFh
big_loop_delay:
	push cx
	;mov cx,050h
	mov cx,032h
small_loop_delay:
	loop small_loop_delay
	pop cx
	loop big_loop_delay
	
	pop cx
ret


; Parameters - nothing
; Create delay of half a second approximately
; Return nothing
timetaker:
	push cx
	
	mov cx,0FFFFh
big_loop:
	push cx
	;mov cx,050h
	mov cx,032h
small_loop:
	loop small_loop
	pop cx
	loop big_loop
	
	pop cx
ret

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ---------------------------------------------------------------------------- Start Game -----------------------------------------------------------------------------------------------
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
start:
	;; initialize data segment register(point to our variables...)
	;mov ax, @data
    ;mov ds, ax
	
	;; prepare game
	call init_game
	;lea  bx, [msg1]     ; the address of or message in dx
	;push bx
	;call prompt_for_start ; press a key to start...
	lea bx,[menu_1]
	;inc bx
	push bx
	call main_menu
	pop cx
	cmp cx,1
	je exit
	;call timetaker ; time taker obviously..
	call prepare_game ; prepare game
	
	call draw_borders
	;;params for init_snake
	lea bx,[snake_pixels] ; get offset address of snake array
	push bx
	mov dx,[length_snake] ; get length of snake(array snake)
	push dx
	mov dx,32000+100 ; get offset of snake in video memory
	push dx
	call init_snake
	
	;;params for spawn first entity
	mov dx,bug2_color
	push dx
	call spawn_entity
	
	mov al,0h
	mov byte [lastmovement],97d
loop_endless:
	mov ah,1h ; SUB FUNCTION INT 16H - (AL=0) Check for key input in keyboard buffer
	int 16h
	jnz GotKey
continue_operations:
	;;params for interact_handler
	lea bx,[snake_pixels]
	push bx ;push offset of array
	mov dx,[length_snake] ; get length of snake(array snake)
	push dx ; push length of array
	push ax ; push key input
	call interact_handler
	pop si
	jmp check_for_interacts
after_interacts:
	; Parameters - Status Integer output(from interact_handler) ,Offset of snake array ,Offset of Length of snake, Offset of Score
	push si
	lea di,[level_score]
	push di
	call level_handler
	;;params for keyboard_handle
	lea bx,[snake_pixels]
	push bx ;push offset of array
	mov dx,[length_snake] ; get length of snake(array snake)
	push dx ; push length of array
	push ax ; push key input
	call keyboard_handle ; otherwise call keyboard handler to check input
	call timetaker
	jmp loop_endless ; continue on endless loop until sentry will take control(AL = 32)

exit:
	call exit_game
    mov ax, 4c00h
    int 21h
check_for_interacts:
	cmp si,color_of_border
	je death_notice_exit
	cmp si,bug1_color
	je increase_size
	cmp si,bug2_color
	je increase_size
	cmp si,poison_color
	je decrease_size
	jmp after_interacts
increase_size:
	lea di,[snake_pixels]
	push di
	lea di,[length_snake]
	push di
	call increase_snake_length
	jmp after_interacts
decrease_size:
	lea di,[snake_pixels]
	push di
	lea di,[length_snake]
	push di
	call decrease_snake_length
	jmp after_interacts

GotKey:
	mov ah,0h ; SUB FUNCTION INT 16H - (AL=0) Get Key
	int 16h
	;; params for valid_action_snake
	xor cx,cx
	mov cl,[lastmovement]
	push cx
	push ax
	call valid_action_snake
	pop ax
	cmp al,32 ; exit on space bar
	je call_main_menu
	mov [lastmovement],al
	continue_got_key:
	mov al,[lastmovement]
	jmp continue_operations
	
death_notice_exit:
	lea bx, [msg_death]
	push bx
	lea bx, [level_score]
	push bx
	call death_msg
	jmp exit
call_main_menu:
	lea bx,[entities_pixels]
	push bx
	lea di,[length_entities]
	push di
	call save_entities
	lea bx,[menu_1]
	;inc bx
	push bx
	call main_menu
	pop si
	cmp si,1
	je exit
	lea bx,[entities_pixels]
	push bx
	lea di,[length_entities]
	push di
	call restore_entities
	call draw_borders
	jmp continue_got_key
	
;;;;;; old code for GotKey....................
;	;;params for interact_handler
;	lea bx,[snake_pixels]
;	push bx ;push offset of array
;	mov dx,[length_snake] ; get length of snake(array snake)
;	push dx ; push length of array
;	push ax ; push key input
;	call interact_handler
;	pop si
;	cmp si,1
;	je death_notice_exit
;	;;params for keyboard_handle
;	lea bx,[snake_pixels]
;	push bx ;push offset of array
;	mov dx,[length_snake] ; get length of snake(array snake)
;	push dx ; push length of array
;	push ax ; push key input
;	call keyboard_handle ; otherwise call keyboard handler to check input
;	call timetaker
;	jmp loop_endless
